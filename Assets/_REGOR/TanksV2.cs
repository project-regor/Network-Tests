using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class TanksV2 : NetworkBehaviour
{
    [Header("Components")]
    public NavMeshAgent agent;
    public Animator animator;

    [Header("Movement")]
    public float rotationSpeed = 100;

    [Header("Firing")]
    public KeyCode shootKey = KeyCode.Space;
    public GameObject projectilePrefab;
    public Transform projectileMount;

    void OnMove(InputValue value)
    {
        Vector2 move = value.Get<Vector2>();
        // rotate
        transform.Rotate(0, move.x * rotationSpeed * Time.deltaTime, 0);

        // move
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        agent.velocity = forward * Mathf.Max(move.y, 0) * agent.speed;
        animator.SetBool("Moving", agent.velocity != Vector3.zero);
    }

    void OnFire()
    {
        CmdFire();
    }
    
    void Update()
    {
        // movement for local player
        if (!isLocalPlayer) return;
    }

    // this is called on the server
    [Command]
    void CmdFire()
    {
        GameObject projectile = Instantiate(projectilePrefab, projectileMount.position, transform.rotation);
        NetworkServer.Spawn(projectile);
        RpcOnFire();
    }

    // this is called on the tank that fired for all observers
    [ClientRpc]
    void RpcOnFire()
    {
        animator.SetTrigger("Shoot");
    }
}
